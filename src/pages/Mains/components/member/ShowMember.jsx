import React from 'react';
import PieChart from '@pages/Mains/components/charts/pie.jsx';

export default function ShowMember(props) {
  console.log('ShowMember render');
  return (
    <div {...props}>
      <PieChart />
    </div>
  )
}
