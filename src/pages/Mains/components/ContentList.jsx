import React , { useState, useRef, useEffect, useMemo }from 'react';
import { Tabs } from 'antd';
import { DndProvider, useDrag, useDrop } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import MyApp from '@pages/Mains/components/MyApp';

const { TabPane } = Tabs;

const initialPanes = [
  {
    title: '我的应用',
    content: MyApp,
    key: 0,
    closable: false,
  },
];
const Memo = React.memo(ContentBarCon);

const type = 'DraggableTabNode';

const DraggableTabNode = ({ index, children, moveNode }) => {
  const ref = useRef(null);
  const [{ isOver, dropClassName }, drop] = useDrop({
    accept: type,
    collect: (monitor) => {
      console.log(monitor);
      const { index: dragIndex } = monitor.getItem() || {};
      if (dragIndex === index) {
        return {};
      }
      return {
        isOver: monitor.isOver(),
        dropClassName: 'dropping',
      };
    },
    drop: (item) => {
      moveNode(item.index, index);
    },
  });
  const [, drag] = useDrag({
    type,
    item: { index, },
    collect: (monitor) => {
      console.log(monitor);
      return {
        isDragging: monitor.isDragging(),
      }
    }
  });
  drop(drag(ref));
  return (
    <div ref={ref} style={{ marginRight: 1, }} className={isOver ? dropClassName : ''}>
      {children}
    </div>
  );
};

const moveTabNode = (dragKey, hoverKey) => {
  console.log(dragKey, hoverKey);
}

const renderTabBar = (tabBarProps, DefaultTabBar) => (
  <DefaultTabBar {...tabBarProps}>
    {(node) => (
      <DraggableTabNode key={node.key} index={node.key} moveNode={moveTabNode}>
        {node}
      </DraggableTabNode>
    )}
  </DefaultTabBar>
);

const DraggableTabs = (props) => {
  return (
    <DndProvider backend={HTML5Backend}>
      {props.children}
    </DndProvider>
  );
};

function ParentsRerender(ActiveKey, setActiveKey, setPanes ,props) {
  let length = initialPanes.length;
  for (let i = 0; i < length; i++) {
    let value = initialPanes[i];
    if(value.title === props.title) {
      if(window.parseInt(ActiveKey.key) === initialPanes[i].key) {
        break;
      }
      setActiveKey({key: initialPanes[i].key});
      break;
    } else if((i === length - 1) && (initialPanes[initialPanes.length - 1].title !== props.title) && props.content) {
      initialPanes.push({
        title: props.title,
        content: props.content,
        key: window.parseInt(props.id),
      });
      // setPanes(initialPanes);
      setActiveKey({key: window.parseInt(props.id)});
    }
  };
}
function ContentBarCon(props) {
  console.log('ContentBar render');
  const Ref = useRef();
  const [ActiveKey, setActiveKey] = useState({key: initialPanes[0].key});
  const [Panes, setPanes] = useState(initialPanes);
  useEffect(() => {
    Ref.current.querySelector('.ant-tabs-nav-add').style.display = 'none';
    Array.prototype.slice.call(Ref.current.querySelectorAll('.ant-tabs-tab')).forEach((elem) => {
      elem.style.height = '100%';
      elem.style.marginBottom = '0px';
      elem.style.paddingBottom = '0px';
      elem.style.lineHeight = '30px';
      elem.style.paddingTop = '0px';
    });
  });
  // ContentBarCon['record'] !== props && ContentBarCon['record']?.content !== props.content && ParentsRerender(ActiveKey, setActiveKey, setPanes, props);
  ContentBarCon['record'] !== props && ParentsRerender(ActiveKey, setActiveKey, setPanes, props);
  ContentBarCon['record'] = props;
  console.log(Panes === initialPanes)
  console.log(ActiveKey)
  const onChange = (newActiveKey) => {
    setActiveKey({key: window.parseInt(newActiveKey)});
  };
  const remove = (targetKey) => {
    let Index;
    initialPanes.filter((elem, index) => {
      if(elem.key === window.parseInt(targetKey)) {
        Index = index;
        return true;
      }
      return false;
    });
    initialPanes.splice(Index, 1);
    (window.parseInt(targetKey) === ActiveKey.key) && (Index !== initialPanes.length) && setActiveKey({key: initialPanes[Index].key});
    (window.parseInt(targetKey) === ActiveKey.key) && (Index === initialPanes.length) && setActiveKey({key: initialPanes[Index - 1].key});
    (window.parseInt(targetKey) !== ActiveKey.key) && setActiveKey({key: ActiveKey.key});
  };
  const onEdit = (targetKey, action) => {
    if(action === 'remove') {
      remove(targetKey);
    }
  };
  return (
    <div ref={Ref} style={{ height: '100%', display: 'flex', flexDirection: 'column' }} >
      {/* <Tabs type="editable-card" onChange={onChange} activeKey={`${ActiveKey['key']}`} onEdit={onEdit} style={{ height: '5%' }} tabBarStyle={{height: '100%', marginBottom:'0px' }} >
        {Panes.map((pane) => (
          <TabPane tab={pane.title} key={pane.key} closable={pane.closable} />
        ))}
      </Tabs> */}
      <DraggableTabs>
        <Tabs type="editable-card" onChange={onChange} activeKey={`${ActiveKey['key']}`} onEdit={onEdit} style={{ height: '5%' }} tabBarStyle={{height: '100%', marginBottom:'0px', paddingBottom: '0px', lineHeight: '30px', paddingTop: '0px' }} renderTabBar={renderTabBar} >
          {Panes.map((pane) => (
            <TabPane tab={pane.title} key={pane.key} closable={pane.closable} style={{height: '100%', marginBottom:'0px', paddingBottom: '0px', lineHeight: '30px', paddingTop: '0px' }} />
          ))}
        </Tabs> 
      </DraggableTabs>
      <ContentCon con={initialPanes} cur={ActiveKey.key} />
    </div>
  )
}

function ContentCon(props) {
  return (
    <div style={{ height: '95%', padding: '0px 17px 17px 17px' }}>
      {props.con.map((elem) => {
        let Wrapper = React.memo(elem.content);
        console.log(props.cur === elem.key)
        return <Wrapper key={elem.key} style={{ display: props.cur === elem.key ? 'block' : 'none', height: '100%' }} />
      })}
    </div>
  )
}

export default function ContentList(props) {
  
  return (
    <>
      <Memo {...props} />
    </>
  )
}
