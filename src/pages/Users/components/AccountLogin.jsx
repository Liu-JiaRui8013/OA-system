import React from 'react';
import { UserOutlined, KeyOutlined } from '@ant-design/icons';

export default function AccountLogin(props) {
  return (
    <div>
      <props.FormItem
        name="username"
        rules={[
          {
            required: true,
            pattern: /([^\s])/,
            message: '用户名不能为空',
          },
          {
            min: 5, max: 10,
            message: '用户名长度必须在5-10之间',
          },
          
        ]}
      >
        <props.Input placeholder="请输入用户名" prefix={<UserOutlined />} onPressEnter={() => {console.log(1)}} allowClear />
      </props.FormItem>

      <props.FormItem
        name="password"
        rules={[
          {
            required: true,
            message: '密码不能为空',
          },
          {
            pattern: /^(\w){6,20}$/,
            message: '密码只能输入6-20个字母、数字、下划线',
          },
        ]}
      >
        <props.Input.Password placeholder="请输入密码" prefix={<KeyOutlined />} />
      </props.FormItem>

      {/* <props.FormItem
        name="remember"
        valuePropName="checked"
        wrapperCol={{
          offset: 8,
          span: 16,
        }}
      >
        <props.Checkbox style={{color: '#e8e5e5'}}>Remember me</props.Checkbox>
      </props.FormItem> */}
   
    </div>
  )
}
