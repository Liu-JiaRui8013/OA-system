import React from 'react';
import { Layout } from 'antd';
import HeaderBar from '@pages/Mains/components/HeaderBar';
import WithComb from '@pages/Mains/HOC/withComb';
import FooterBar from '@pages/Mains/components/FooterBar';
import TopBg from '@assets/imgs/top_bg.png';

const { Header, Footer } = Layout;

export default function MainPage() {
  
  return (
    <>
      <Layout style={{ height : '100%', minWidth : '1200px' }}>
        <Header style={{ backgroundColor : '#fff', padding: '0', background : `url(${TopBg})`, lineHeight: '0px' }}>
          <HeaderBar />
        </Header>
        <WithComb />
        <Footer style={{ backgroundColor : '#fff', padding : '10px 45px 10px 45px', height: '56px', lineHeight: '36px' }}>
            <FooterBar />
        </Footer>
      </Layout>
    </>
  )
}
