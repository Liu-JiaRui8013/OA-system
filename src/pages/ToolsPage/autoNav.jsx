import React from 'react';
import { Navigate } from 'react-router-dom';

export default function AutoNav(props) {
  return (
    <>
      <Navigate to={props.target} replace></Navigate>
    </>
  )
}
