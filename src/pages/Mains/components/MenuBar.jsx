import React, { useState } from 'react';
import {
  AppstoreOutlined,
  ContainerOutlined,
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  PieChartOutlined,
  DesktopOutlined,
  MailOutlined,
} from '@ant-design/icons';
import { Button, Menu } from 'antd';
import AddressBook from '@pages/Mains/components/affairs/AddressBook';
import Attendance from '@pages/Mains/components/affairs/Attendance';
import CtrlPanel from '@pages/Mains/components/affairs/CtrlPanel';
import Mail from '@pages/Mains/components/affairs/Mail';
import Note from '@pages/Mains/components/affairs/Note';
import NotiMsg from '@pages/Mains/components/affairs/NotiMsg';
import UnreadMatter from '@pages/Mains/components/affairs/UnreadMatter';
import ShowDepartment from '@pages/Mains/components/department/ShowDepartment';
import ShowMember from '@pages/Mains/components/member/ShowMember';
import ShowPosition from '@pages/Mains/components/position/ShowPosition';
// import MyApp from '@pages/Mains/components/MyApp';
// import MyApp from '@pages/Mains/components/MyApp';
// import MyApp from '@pages/Mains/components/MyApp';
// import MyApp from '@pages/Mains/components/MyApp';
// import MyApp from '@pages/Mains/components/MyApp';
// import MyApp from '@pages/Mains/components/MyApp';
// import MyApp from '@pages/Mains/components/MyApp';
// import MyApp from '@pages/Mains/components/MyApp';

let compMapping = {};
function getItem(comp, label, key, icon, children, type) {
  comp && (compMapping[key] = {comp, label});
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}

const items = [
  getItem(ShowMember, '员工', '1', <PieChartOutlined />),
  getItem(ShowDepartment, '部门', '2', <DesktopOutlined />),
  getItem(ShowPosition, '职级', '3', <ContainerOutlined />),
  getItem(null, '个人事务', 'sub1', <AppstoreOutlined />, [
    getItem(Mail, '电子邮件', '4'),
    getItem(UnreadMatter, '待阅事宜', '5'),
    getItem(NotiMsg, '公告通知', '6'),
    getItem(Attendance, '个人考勤', '7'),
    getItem(AddressBook, '通讯录', '8'),
    getItem(Note, '便签', '9'),
    getItem(CtrlPanel, '控制面板', '10'),
  ]),
  getItem(null, '人力资源', 'sub2', <MailOutlined />, [
    getItem(null, '考勤管理', 'sub3', null, [getItem(null, '考勤审批', '11'), getItem(null, '考勤统计', '12'), getItem(null, '考勤设置', '13')]),
    getItem(null, '绩效考核', 'sub4', null, [getItem(null, '考核项目设定', '14'), getItem(null, '考核任务管理', '15'), getItem(null, '评分汇总', '16')]),
    getItem(null, '足迹查询', '17'),
    getItem(null, '薪酬管理', 'sub5', null, [getItem(null, '工资管理', '18'), getItem(null, '工资查询设置', '19')]),
  ]),
  
];

export default function MenuBar(props) {
  // let [collapsed, setCollapsed] = useState(false);
  let [sub, setSub] = useState([]);
  return (
    <>
      <Button
        type="primary"
        // onClick={props.setCollapsed}
        onClick={(e) => {e.currentTarget.blur(); setSub([]); props.setCollapsed(!props.collapsed);}}
        // onClick={() => {
        //   setCollapsed(!collapsed)
        // }}
        style={{
          height: '5%',
          width: '50px',
          backgroundColor : 'rgba(32, 161, 222)',
          borderColor : 'rgba(32, 161, 222)'
          // position: 'fixed',
          // top: '0px',
          // left: '200px',
          // height: '64px',
          // opacity: .8,
          // backgroundColor : 'rgba(35, 161, 221)'
        }}
      >
        {props.collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
      </Button>
      <Menu
        // style={{
          // height: '95%',
        // }}
        // defaultSelectedKeys={['1']}
        // defaultOpenKeys={['sub1']}
        mode="inline"
        theme="dark"
        // inlineCollapsed={props.collapsed}
        items={items}
        onClick={(info) => {
          console.log(info);
          props.setContentInfo({content: compMapping[info.key].comp, title: compMapping[info.key].label, key: info.key});
        }}
        onOpenChange={(subMenu) => {
          console.log(subMenu);
          // sub = subMenu
          // collapsed ? setSub(["sub1"]) : setSub(subMenu)
          setSub(subMenu);
        }}
        openKeys={sub}
      />
    </>
  );
}
