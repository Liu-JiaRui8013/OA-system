export default function getPosition() {
  return new Promise((resolve, reject) => {
    function getPositionInfo(position) {
      resolve(position);
    }
    function getPositionErr(error) {
      switch (error.code) {
        case error.PERMISSION_DENIED:
          console.warn(error);
          reject(error);
          break;
        default:
          console.warn(error);
          reject(error);
      }
    }
    if (navigator.geolocation) {
      console.log(1);
      navigator.geolocation.getCurrentPosition(getPositionInfo, getPositionErr);
    } else {
      console.warn('Not support geolocation');
      reject('Not support geolocation');
    }
  });
}
