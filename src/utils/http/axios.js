import axios from 'axios';
import Qs from 'qs';

let instance = axios.create();
let transHandleMap = new Map();
let transHandleGet = function(data, headers) {
  // console.log(headers);
  // headers.get['Content-Type'] = 'application/json';
  // return Qs.stringify(data);
  // return String.fromCharCode.apply(null, new Uint16Array(data));
  return data;
}
let transHandleOthers = function(data, headers) {
  console.log(headers);
  return data;
}
let restfulMenthods = ['GET', 'POST', 'PUT', 'DELETE'];
for (const value of restfulMenthods) {
  value === 'GET' ? transHandleMap.set(value, transHandleGet) : transHandleMap.set(value, transHandleOthers);
}
export default function Request({URL, Method = 'GET', Data, ...Params}) {
  let transHandle = transHandleMap.get(Method);
  let config = {
    method: Method,
    url: Method === 'GET' ? URL + '?' + Qs.stringify(Data) : URL,
    data: Method === 'GET' ? '' : Data,
    transformRequest: Method === 'GET' ? transHandle : [transHandle],

  };
  return instance.request(config);
}
