import React from 'react';
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { SwitchTransition, CSSTransition } from "react-transition-group";
import Autonav from "@pages/ToolsPage/autoNav";
import Modal from "@pages/Users/components/Modal";
import Userlogin from "@pages/Users/login";
import Mainpage from "@pages/Mains/mainPage";
import Notfound from "@pages/404Page/notFound";
import '@routers/routerStyles/router.less';

export default function BaseRouter() {
  return (
   
      <Router>
         <SwitchTransition>
          <CSSTransition timeout={1000} key={window.location.pathname}>
            <Routes>
              <Route path="/" element={<Autonav {...{target: "/user/login/"}}/>} />
              <Route path="/user/login/" element={<Userlogin {...{name: "ljr"}}/>} />
              <Route path="/user/forgetpwd/" element={<Modal show='forgetpwd'/>} />
              <Route path="/user/register/" element={<Modal show='register'/>} />
              <Route path="/main/" element={<Mainpage {...{name: "ljr"}}/>} />
              <Route path="*" element={<Notfound />} />
            </Routes>
            </CSSTransition>
          </SwitchTransition>
      </Router>
       
  )
}
