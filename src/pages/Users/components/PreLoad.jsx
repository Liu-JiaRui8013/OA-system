import React, {useState} from 'react';
import Box from '@pages/Users/styles/box.module.less';
import LoginPreBg from '@assets/imgs/login_bg_pre.jpg';
import LoginBg from '@assets/imgs/login_bg.jpg';

export default function PreLoad(props) {
  let [Loaded, setLoaded] = useState(false);
  return (
    Loaded ? <img src={LoginBg} alt="login_bg" className={Box.login_bg} onLoad={() => {
      console.log(1);
      setLoaded(true);
    }} /> : 
    <>
      <img src={LoginBg} alt="login_bg" className={Box.login_bg} style={{display: 'none'}} onLoad={() => {
        setTimeout(() => {
          setLoaded(true);
        }, 500);
        console.log(1);
      }} />
      <img src={LoginPreBg} alt="login_bg" className={Box.login_bg} />
    </>
    
  )
}
