import React from 'react';
import { PhoneOutlined, MessageOutlined } from '@ant-design/icons';
import { Col, Row, Button } from 'antd';

export default function SmsCodeLogin(props) {
  return (
    <div>
      <props.FormItem
        name="number"
        rules={[
          {
            required: true,
            pattern: /([^\s])/,
            message: '手机号码不能为空',
          },
          {
            pattern: /^(\d){11}$/,
            message: '请填入正确的手机号码',
          },
          
        ]}
      >
        <props.InputNumber style={{width: '100%'}} controls={false} placeholder="请输入您的手机号码" prefix={<PhoneOutlined />} onPressEnter={() => {console.log(1)}} />
      </props.FormItem>

      <Row style={{marginBottom: '24px'}}>
        <Col span={9} offset={4} style={{height: '32px'}}>
          <props.FormItem
          name="code"
          rules={[
            {
              required: true,
              pattern: /^(\d){6}$/,
              message: '请输入六位验证码',
            },
          ]}
          wrapperCol={{
            offset: 0,
            span: 24,
          }}
          >
            <props.InputNumber style={{width: '100%'}} controls={false} placeholder="请输入验证码" prefix={<MessageOutlined />} />
          </props.FormItem>
        </Col>
        <Col span={7} offset={0}>
          <Button type="primary" onClick={() => {console.log("click btn")}}>
            发送验证码
        </Button>
        </Col>
      </Row>
    </div>
  )
}
