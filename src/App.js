import React from "react";
import RouteContainer from "@routers/routerBase/router";
import RouteDefencer from "@routers/routerDefence/router";

function App() {
  return (
    <div className="container" style={{height: '100%'}} >
      <RouteDefencer>
        <RouteContainer></RouteContainer>
      </RouteDefencer>
    </div>
  );
}

export default App;
