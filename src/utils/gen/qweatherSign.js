const md5 = require('md5');
export const privateKey = "7a52fa81fc294f15b894983990347fc1";
function extArr(arr, i, value) {
  for (let k = arr.length; k > i - 1; k--) {
    if (k === i) {
      arr[k] = value;
      break;
    }
    arr[k] = arr[k - 1];
  }
}
// 按文档需求拼接字符串
function putInOrder(key, arr) {
  if (arr.length === 0) {arr.push(key); return;}
  let Key = key;
  let Length = arr.length;
  key = String.prototype.toLowerCase.call(key);
  for (let i = 0; i < Length; i++) {
    for (let j = 0; j < key.length; j++) {
      if (String.prototype.charCodeAt.call(key, j) > String.prototype.charCodeAt.call(String.prototype.toLowerCase.call(arr[i]), j)) {
        break;
      } else if (String.prototype.charCodeAt.call(key, j) === String.prototype.charCodeAt.call(String.prototype.toLowerCase.call(arr[i]), j)) {
        if (j === key.length - 1) {
          extArr(arr, i, Key);
          return;
        }
        // continue;
      } else {
        extArr(arr, i, Key);
        return;
        // for (let k = arr.length + 1; k > i - 1; k--) {
        //   if (k === i) {
        //     arr[k] = Key;
        //     break;
        //   }
        //   arr[k] = arr[k - 1];
        // }
      }
    }
    if (i === Length - 1) arr[i + 1] = Key;
  }
}
export default function qweatherSign(data) {
  let OrderedArr = [];
  let ParamsString = '';
  let Nums = 0;
  let SignString = '';
  for (let key in data) {
    putInOrder(key, OrderedArr);
  }
  for (let value of OrderedArr) {
    Nums++;
    ParamsString += Nums === OrderedArr.length ? `${value}=${data[value]}` : `${value}=${data[value]}&`;
  }
  SignString = md5(ParamsString + privateKey);
  return SignString;
}
// Test func;
// console.log(qweatherSign({ac:1,abb:2,bcdd:1, Adjb:2, RUI:3, DIjb:2, dimeeefvz:3})); 
