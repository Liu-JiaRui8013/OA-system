import React, { useState, useEffect, useRef } from 'react';
import { Space, Spin } from 'antd';
import GetPosition from '@utils/geo/getposition.js';
import * as GetWeatherSign from '@utils/gen/qweatherSign.js';
import Request from '@utils/http/axios.js';
import GadgetStyles from '@pages/Mains/styles/gadget.module.less';
import HeaderBarStyles from '@pages/Mains/styles/headerbar.module.less';
import CompanyLogo from '@assets/imgs/xindaOA.png';

let GadgetList = {
  weather: WeatherGadget,
  time: TimeGadget
};
let DateMap = {
  0: '天',
  1: '一',
  2: '二',
  3: '三',
  4: '四',
  5: '五',
  6: '六',
};
function GadgetComponent(props) {
  let controllerList = {};
  // let [Loaded, setLoaded] = useState(false);

  // setInterval(() => {
  //   setLoaded(!Loaded)
  // }, 1000);
  // 清理函数
  useEffect(() => {
  // a = 2
    // 优化时间准确度
    // console.log('abc')
    let Timer, WeatherTimer;
    let InitTimer = setInterval(() => {
      Timer = setInterval(() => {
        controllerList['time'].handler((new Date()).getSeconds());
      }, 1000);
      // WeatherTimer = setInterval(() => {
      //   controllerList['weather'].handler1(false);
      //   controllerList['weather'].handler(true);
      // }, 10000);
      controllerList['time'].handler((new Date()).getSeconds());
      clearInterval(InitTimer);
    }, 1000 - new Date().getMilliseconds());
    return () => {
    // console.log('abcd ')
      clearInterval(Timer);
      // clearInterval(WeatherTimer);
    }
  });
  return (
    <>
      {props.list.map((item, index) => {
        let Elem = GadgetList[item];
        controllerList[item] = {handler: null, handler1: null};
        return <Elem key={index} controller={controllerList[item]}/>;})
      }
    </>
  )
}
// let a;
// let a;
function WeatherGadget(props) {
  let [Loaded, setLoaded] = useState(false);
  // let [GeoInfo, setGeoInfo] = useState(null);
  let [IsFetch, setIsFetch] = useState(false);
  // setIsFetch(!IsFetch)
  // setLoaded(!Loaded)
  let Ref = useRef();
  // setTimeout(() => {
  //   console.log(a === Ref)
  // }, 5000);
  props.controller.handler = setIsFetch;
  props.controller.handler1 = setLoaded;
  console.log(Loaded, IsFetch)
  // let TotalData;
  useEffect(() => {
  // a = Ref
  // console.log(Ref)
    if (IsFetch) {setIsFetch(false);}
    GetPosition().then((result) => {
      let LocationConf, LocationSign, GeoInfo;
      GeoInfo = result;
      // setGeoInfo(result);
      LocationConf = {location: `${GeoInfo.coords.longitude},${GeoInfo.coords.latitude}`, key: GetWeatherSign.privateKey};
      LocationSign = GetWeatherSign.default(LocationConf);
      let reqList = [];
      let reqData = {...LocationConf, sign: LocationSign}
      let reqConf = [{URL: 'https://geoapi.qweather.com/v2/city/lookup', Method: 'GET', Data: reqData}, 
      {URL: 'https://devapi.qweather.com/v7/weather/now', Method: 'GET', Data: reqData}, 
      {URL: 'https://devapi.qweather.com/v7/air/now', Method: 'GET', Data: reqData}];
      for (const value of reqConf) {
        reqList.push(Request(value));
      }
      Promise.all(reqList).then((result) => {
        console.log(result);
        Ref.current = {
          city: result[0]['data']['location'][0].adm2,
          temp: result[1]['data']['now']['temp'],
          icon: result[1]['data']['now']['icon'],
          air_quality: result[2]['data']['now']['aqi'],
          air_level: result[2]['data']['now']['category'],
        }
        setLoaded(true);
      });
    }).catch((reason) => {
      console.log(reason);
    });
  }, [IsFetch]);
  return (
    <>
      {Loaded ? 
      <Space className={GadgetStyles.container} size={12}>
      <span>{Ref.current.city}</span>
      <span><i className={`qi-${Ref.current.icon}`}></i></span>
      <span>{`${Ref.current.temp}℃`}</span>
      <span>{Ref.current.air_level}</span>
      <span>{Ref.current.air_quality}</span>
      </Space> : <Spin />}
    </>
  )
}

function TimeGadget(props) {
  // console.log(a === props);
  // a = props;
  let time = new Date();
  let [sec, setSec] = useState(time.getSeconds());
  props.controller.handler = setSec;
  let month = time.getMonth();
  let timeList = {
    month: month + 1 < 10 ? '0' + (month + 1) : month + 1,
    date: time.getDate(),
    day: DateMap[time.getDay()],
    hour: time.getHours(),
    minute: time.getMinutes() < 10 ? `0${time.getMinutes()}` : time.getMinutes(),
    second: sec < 10 ? `0${sec}` : sec,
  };
  return (
    <>
      <Space className={GadgetStyles.container}>
        <span>{timeList.month}月{timeList.date}日</span>
        <span>星期{timeList.day}</span>
        <span>{timeList.hour}:{timeList.minute}:{timeList.second}</span>
      </Space>
    </>
  )
}
// let b1;
// function Test(props) {
//   let c = {}
//   let [sec1, setSec1] = useState(c);
//   console.log(sec1 === c)
//   console.log(sec1 === a)
//   console.log(sec1 === b1)
//   a = sec1;
//   let b = {b:1}
//   useEffect(() => {
//     setSec1(b)
//     b1 = b;
//   }, [0])
// }
export default function HeaderBar() {
  return (
    <div className={HeaderBarStyles.container}>
      <div className={HeaderBarStyles.logo}>
        <img src={CompanyLogo} alt="信达OA" />
      </div>
      <div className={HeaderBarStyles.comp}>
        <GadgetComponent {...{list: ['time', 'weather']}} />
        {/* <Test /> */}
      </div>
    </div>
  )
}
