import React, { useRef, useEffect } from 'react';
import * as echarts from 'echarts/core';
import { TitleComponent, TooltipComponent, LegendComponent } from 'echarts/components';
import { PieChart } from 'echarts/charts';
import { LabelLayout } from 'echarts/features';
import { CanvasRenderer } from 'echarts/renderers';

echarts.use([
  TitleComponent,
  TooltipComponent,
  LegendComponent,
  PieChart,
  CanvasRenderer,
  LabelLayout
]);

export default function Pie(props) {
  let Ref = useRef();
  useEffect(() => {
    let myChart = echarts.init(Ref.current);
    let option;
    option = {
      tooltip: {
        trigger: 'item'
      },
      title: {
        text: "员工数分布图",
        left: "center",
        top: "bottom",
        textStyle: {
          fontSize: 18
        },
      },
      legend: {
        top: '5%',
        left: 'center'
      },
      series: [
        {
          name: '员工分布',
          type: 'pie',
          radius: ['50%', '70%'],
          avoidLabelOverlap: false,
          itemStyle: {
            borderRadius: 10,
            borderColor: '#fff',
            borderWidth: 2
          },
          label: {
            show: false,
            position: 'center'
          },
          emphasis: {
            label: {
              show: true,
              fontSize: '15',
              fontWeight: 'bold'
            }
          },
          labelLine: {
            show: false
          },
          data: [
            { value: 48, name: '销售' },
            { value: 35, name: '财务' },
            { value: 80, name: '人事' },
            { value: 84, name: '采购' },
            { value: 30, name: '加工' }
          ],
          top: 15,
        }
      ]
    };
    option && myChart.setOption(option);
  });
  return (
    <div className="pie_container" ref={Ref} style={{ width: '350px', height: '240px' }}>

    </div>
  )
}
