import React from 'react';
import PropTypes from 'prop-types';
import { Space } from 'antd';
import { createFromIconfontCN } from '@ant-design/icons';
import Align from '@pages/Users/styles/align.module.less';

export default function Icon(props) {
  const IconFont = createFromIconfontCN({
    scriptUrl: props.scriptUrl,
  });
  return (
    <Space className={Align.root}>
      <div>
      {typeof props.items === 'object' ? props.items.map((item, index) => 
        <IconFont type={item} key={index} style={{fontSize: props.size + 'px'}} />) : <IconFont type={props.items} />}
      </div>
    </Space>
  )
}
Icon.propTypes = {
  scriptUrl: PropTypes.string.isRequired,
  items: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.array
  ]).isRequired,
  size: PropTypes.number.isRequired,
}
