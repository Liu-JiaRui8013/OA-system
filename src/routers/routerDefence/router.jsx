import React, { useState } from "react";
import {
  BrowserRouter as Router,
  Route,
  Routes,
  Navigate,
} from "react-router-dom";
import Userlogin from "@pages/Users/login";

let ChangeState, State;
export function ControlAuth() {
  return {
    logout() {
      return new Promise((resolve) => {
        ChangeState(0);
        resolve();
      });
    },
    login() {
      return new Promise((resolve) => {
        ChangeState(1);
        resolve();
      });
    },
  };
}

export default function RouterDefence({ children }) {
  [State, ChangeState] = useState(1);
  return State ? (
    children
  ) : (
    <div>
      <Router>
        <Routes>
          <Route
            path="/user/login"
            element={<Userlogin {...{ name: "ljr" }} />}
          />
          <Route
            path="*"
            element={<Navigate to="/user/login" replace></Navigate>}
          />
        </Routes>
      </Router>
    </div>
  );
}
