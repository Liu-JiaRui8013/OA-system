import React from 'react';

export default function MyApp(props) {
  return (
    <div {...props}>MyApp</div>
  )
}
