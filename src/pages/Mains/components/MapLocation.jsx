import React, { useRef, useEffect } from 'react';
import { Map, Marker, NavigationControl, InfoWindow } from 'react-bmapgl';

export default function MapLocation() {
  let ref = useRef();
  useEffect(() => {
    console.log(ref);
  })
  let BMapGL = window.BMapGL;
  let BMAP_STATUS_SUCCESS = window.BMAP_STATUS_SUCCESS;
  let map = new BMapGL.Map("map");
  let point = new BMapGL.Point(116.331398,39.897445);
  map.centerAndZoom(point,12);
  
  let geolocation = new BMapGL.Geolocation();
  geolocation.getCurrentPosition(function(r){
    if(this.getStatus() === BMAP_STATUS_SUCCESS){
        var mk = new BMapGL.Marker(r.point);
        map.addOverlay(mk);
        map.panTo(r.point);
        alert('您的位置：' + r.point.lng + ',' + r.point.lat);
    }
    else {
        alert('failed' + this.getStatus());
    }     
  });
  return (
    <>
      <Map center={{lng: 116.402544, lat: 39.928216}} zoom="11">
            <Marker position={{lng: 116.402544, lat: 39.928216}} />
            <NavigationControl /> 
            <InfoWindow position={{lng: 116.402544, lat: 39.928216}} text="内容" title="标题" />
      </Map>
    </>
  )
}
