import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import Userforgetpwd from "@pages/Users/forgetPassword";
import Userregister from "@pages/Users/register";
import PreLoad from '@pages/Users/components/PreLoad';
import { PlusOutlined, LeftOutlined } from '@ant-design/icons';
import { Button, Cascader, Col, DatePicker, Drawer, Form, Input, Radio, Row, Select } from 'antd';
import Box from '@pages/Users/styles/box.module.less';
const { Option } = Select;

function SexPicker() {
  let [value, setValue] = useState(1);
  let onChange = (e) => {
    setValue(e.target.value);
  };
  return (
    <Form.Item
      name="sex"
      label="性别"
      rules={[
        {
          required: true,
          message: '请选择您的性别',
        },
      ]}
    >
      <Radio.Group onChange={onChange} value={value}>
        <Radio value={1}>男</Radio>
        <Radio value={0}>女</Radio>
      </Radio.Group>
    </Form.Item>
  )
}

let options = [
  {
    value: 'zhejiang',
    label: 'Zhejiang',
    children: [
      {
        value: 'hangzhou',
        label: 'Hangzhou',
        children: [
          {
            value: 'xihu',
            label: 'West Lake',
          },
        ],
      },
    ],
  },
  {
    value: 'jiangsu',
    label: 'Jiangsu',
    children: [
      {
        value: 'nanjing',
        label: 'Nanjing',
        children: [
          {
            value: 'zhonghuamen',
            label: 'Zhong Hua Men',
          },
        ],
      },
    ],
  },
];

Modal['record'] = false;
export default function Modal(props) {
  let [visible, setVisible] = useState(false);
  let Nav = useNavigate();
  useEffect(() => {
    visible && (document.body.style.overflow = 'auto');
    visible && (document.body.querySelector('.ant-drawer').style.position = 'absolute');
    Modal['record'] && !visible && (document.body.querySelector('.ant-drawer').style.position = 'fixed');
    Modal['record'] = visible;
  });
  let showDrawer = () => {
    setVisible(true);
  };
  let onClose = () => {
    setVisible(false);  
  };
  let onSubmit = (values) => {
    console.log(values);
  };
  let onSelect = (date, dateString) => {
    console.log(date, dateString);
  }
  let onChange = (value) => {
    console.log(value);
  };
  
  return (
    <div className={Box.root} >
      <PreLoad />
      <Button type="primary" onClick={() => {
        Nav("/user/login")
      }} icon={<LeftOutlined />}>
        返回登陆
      </Button>
      <Button type="primary" onClick={showDrawer} icon={<PlusOutlined />} style={{ marginLeft: '2px' }}>
        创建用户
      </Button>
      <Drawer
        title="创建新账号"
        width={600}
        onClose={onClose}
        visible={visible}
        bodyStyle={{
          paddingBottom: 80,
        }}
      >
        <Form layout="vertical" hideRequiredMark onFinish={onSubmit}>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="role"
                label="角色"
                rules={[
                  {
                    required: true,
                    message: '请选择一个角色',
                  },
                ]}
              >
                <Select placeholder="请选择一个角色">
                  <Option value="sale">销售</Option>
                  <Option value="finance">财务</Option>
                  <Option value="personal">人事</Option>
                  <Option value="purchase">采购</Option>
                  <Option value="manufacture">加工</Option>
                </Select>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="name"
                label="帐号名"
                rules={[
                  {
                    required: true,
                    message: '请输入帐号名',
                  },
                ]}
              >
                <Input placeholder="请输入帐号名" />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="password"
                label="密码"
                rules={[
                  {
                    required: true,
                    message: '请输入正确格式的密码',
                  },
                ]}
              >
                <Input.Password placeholder="请输入您的密码" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="addr"
                label="地址"
                rules={[
                  {
                    required: true,
                    message: '请选择您的地址',
                  },
                ]}
              >
                <Cascader options={options} onChange={onChange} placeholder="请选择您的地址" />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="phone"
                label="手机号码"
                rules={[
                  {
                    required: true,
                    message: '请输入正确的手机号码',
                  },
                ]}
              >
                <Input placeholder="请输入手机号码" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="mail"
                label="邮箱"
                rules={[
                  {
                    required: true,
                    message: '请输入正确的邮箱格式',
                  },
                ]}
              >
                <Input placeholder="请输入邮箱地址" />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}>
              <SexPicker />
            </Col>
            <Col span={12}>
              <Form.Item
                name="birthday"
                label="出生日期"
                rules={[
                  {
                    required: true,
                    message: '请选择您的生日',
                  },
                ]}
              >
                <DatePicker
                  style={{
                    width: '100%',
                  }}
                  getPopupContainer={(trigger) => trigger.parentElement}
                  onChange={onSelect}
                  placeholder="请选择您的生日"
                />
              </Form.Item>
            </Col>
          </Row>

          <Form.Item>
            <Row>
              <Col span={4} offset={10}>
                <Button type="primary" htmlType="submit" block>
                  提交
                </Button>
              </Col>
            </Row>
          </Form.Item>
        </Form>
      </Drawer>
    </div>
  );
};