import React, { useState } from 'react';
import { Layout } from 'antd';
import { useParams } from 'react-router';
import ContentList from '@pages/Mains/components/ContentList';
import MenuBar from '@pages/Mains/components/MenuBar';

const { Sider, Content } = Layout;

WithComb['Isrender'] = false;
export default function WithComb() {
  let params = useParams();
  console.log(params);
  let [collapsed, setCollapsed] = useState(false);
  let [contentInfo, setContentInfo] = useState({});
  // let collapsed = false;
  if((WithComb['collapsed'] === true && collapsed === false) || (WithComb['collapsed'] === false && collapsed === true)) {
  } else {
    WithComb['Isrender'] = !WithComb['Isrender'];
  }
  WithComb['collapsed'] = collapsed;
  return (
    <Layout style={{ minHeight : '580px' }}>
      <Sider collapsed={collapsed} collapsedWidth={50}
        style={{ overflowY : 'auto', backgroundColor : 'rgb(0, 21, 41)' }}
      >
        {/* <MenuBar setCollapsed={() => {collapsed = !collapsed}} collapsed={collapsed}></MenuBar> */}
        <MenuBar setCollapsed={setCollapsed} collapsed={collapsed} setContentInfo={setContentInfo} />
        {/* <MenuBar></MenuBar> */}
      </Sider>
      <Content style={{ backgroundColor : '#fff'}}>
        <ContentList content={contentInfo.content} title={contentInfo.title} id={contentInfo.key} Isrender={WithComb['Isrender']} />
      </Content>
    </Layout>
  )
}
