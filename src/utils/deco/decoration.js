 //装饰器
//  const changeParams = () => {  
//   //  三个参数分别是：装饰的方法的类本身（可以理解为class的this）、装饰的方法名称、可以理解为Object.defineProperty()
//    return (target, name, desc) => {  
//      const fun = desc.value   //保存一份源方法
//      desc.value = function (params) {   //这里获取到原方法的参数！也可以用 arguments 取得全部参数
//          //do something 
//         return fun(params)
//      }
//    }
//  }
 
//  class test {
//    //  放在类里面的方法上面
//    @changeParams ()    
//       test() {
//         console.log(1);
//    } 
//  }
 