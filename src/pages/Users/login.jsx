import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import GenscriptUrl from '@utils/gen/scriptUrl';
import AccountLogin from '@pages/Users/components/AccountLogin';
import SmsCodeLogin from '@pages/Users/components/SmsCodeLogin';
import PreLoad from '@pages/Users/components/PreLoad';
import Icon from '@pages/Users/components/Icon';
import { Form, Input, Button, Checkbox, Col, Row, InputNumber } from 'antd';
// import 'antd/dist/antd.css';
import Box from '@pages/Users/styles/box.module.less';

export default function Login() {
  let [form] = Form.useForm();
  let FormItem = Form.Item;
  // let Ref = React.useRef();
  let [State, SetN] = useState(0);
  let Nav = useNavigate();
  let JudgeComps = props => !State ? <AccountLogin {...props} /> : <SmsCodeLogin {...props} />;
  let onFinish = (values) => {
    Nav("/main/");
    console.log('Success:', values);
  };
  let onFinishFailed = (errorInfo) => {

    console.log('Failed:', errorInfo);
  };
  return (
    <div className={Box.root} >
      {/* <img src={LoginBg} alt="login_bg" className={Box.login_bg} ref={Ref} onLoad={() => {
        Ref.current.style.width = (document.documentElement.clientWidth || document.body.clientWidth) + "px";
        Ref.current.style.height = (document.documentElement.clientHeight || document.body.clientHeight) + "px";
      }}/> */}
      <PreLoad />
      <div className={Box.login}>
        <div className={Box.title}>
          <Icon scriptUrl={GenscriptUrl()} items={["icon-OAxitong"]} size={30} />
          <span className={Box.company_name}>信达网络智能办公系统</span>
        </div>
        <Form
        name="basic"
        form={form}
        wrapperCol={{
          offset: 4,
          span: 16,
        }}
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        {JudgeComps({form, FormItem, Input, Checkbox, InputNumber})}
          <FormItem>
            <Row className={Box.otherfunc} align='bottom'>
                <Col span={16} offset={4}>
                  <Button type="primary" htmlType="submit" className={Box.botton} block>
                    登录
                  </Button>
                </Col>
              </Row>
          </FormItem>
        </Form>
        <Row className={Box.otherfunc} align='bottom'>
          <Col span={4} offset={4}>
            <span className={`${Box.tips}`}>
              <span onClick={() => {
                Nav("/user/register");
              }}>注册账户?</span>
               <span onClick={() => {
                Nav("/user/forgetpwd");
              }}>忘记密码?</span>
              </span>
          </Col>
          <Col span={12} offset={4}>
            <span className={Box.tips} onClick={() => {
              SetN(!State);
            }}>
              {!State ? <span>使用短信验证码进行登录</span> : <span>使用账户名密码进行登录</span>}
              <Icon scriptUrl={GenscriptUrl()} items={["icon-xiangyoujiantou"]} size={16} />
            </span>
          </Col>
        </Row>
      </div>
    </div>
  )
}

